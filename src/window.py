# window.py
#
# Copyright 2021 Mikhail Elenskii
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import json
from gi.repository import Gtk, Gio, GLib
from queue import Queue
from typing import Union, Tuple

from .callback_server import CallbackServer
from .globals import *
from .playlist import Playlist
from .user import User
from .token import save_tokens, get_tokens, refresh_tokens


@Gtk.Template(resource_path='/me/mkls/PlaylistBrowser/window.ui')
class SpotifyPlaylistBrowserWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'SpotifyPlaylistBrowserWindow'

    scrolled_window = Gtk.Template.Child()
    treeview = Gtk.Template.Child()
    tree_selection = Gtk.Template.Child()
    headerbar = Gtk.Template.Child()

    dialog = Gtk.Template.Child()

    delete_button = Gtk.Template.Child()
    add_button = Gtk.Template.Child()
    modify_button = Gtk.Template.Child()
    spinner = Gtk.Template.Child()

    name_column = Gtk.Template.Child()
    tracks_count_column = Gtk.Template.Child()

    tracks_renderer = Gtk.Template.Child()
    name_renderer = Gtk.Template.Child()

    playlist_store = Gtk.Template.Child()

    dialog = Gtk.Template.Child()
    dialog_name_entry = Gtk.Template.Child()
    dialog_description_entry = Gtk.Template.Child()
    dialog_apply_button = Gtk.Template.Child()
    dialog_cancel_button = Gtk.Template.Child()
    dialog_vbox = Gtk.Template.Child()
    dialog_box = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.callback_server = CallbackServer(port=8888,
                                              ready_callback=self.tokens_ready)
        self.access_token = None
        self.refresh_token = None
        self.token_counter = 0
        self.first_boot = True
        self.user = None

        self.dialog_add = False # If false, modify existing object

        self.dialog_apply_button.connect('clicked', self.dialog_apply_clicked)
        self.dialog_cancel_button.connect('clicked', self.dialog_cancel_clicked)

        self.call_queue = Queue()

        self.add_button.connect('clicked', self.add_clicked)
        self.delete_button.connect('clicked', self.delete_clicked)
        self.modify_button.connect('clicked', self.modify_clicked)

        self.name_column.set_expand(True)
        self.name_column.set_cell_data_func(
            self.name_renderer,
            self.render_playlist_name
        )

        self.tracks_count_column.set_cell_data_func(
            self.tracks_renderer,
            self.render_track_count
        )

        GLib.timeout_add(300000, refresh_tokens, self)

    def render_playlist_name(self, column, cell, model, it, *data):
        playlist = model.get_value(it, 0)
        cell.set_property('text', playlist.name)

    def render_track_count(self, column, cell, model, it, *data):
        playlist = model.get_value(it, 0)
        cell.set_property('text', str(playlist.tracks['total']))

    def playlist_callback(self, source, result, *user_data):
        self.__switch_spinner__(True)

        res = source.send_finish(result)
        j = read_data_stream(res)
        if j is None:
            logging.error('Failed to retrive playlists')
        else:
            j = json.loads(j)

            for p in j['items']:
                obj = Playlist(p)
                self.playlist_store.append((obj,))

        self.__switch_widgets__(True)

    def load_playlists(self):
        self.__switch_widgets__(False)
        playlists = Playlist.get_playlists(self.access_token,
                                           self.playlist_callback)

    def authenticate(self):
        self.__switch_widgets__(False)
        self.headerbar.set_title('Waiting for authentication')
        self.callback_server.get_token()

    def tokens_ready(self, access_token, refresh_token):
        logging.debug('Tokens are ready')

        self.access_token = access_token
        self.refresh_token = refresh_token

        logging.debug('Saving tokens to keyring')
        save_tokens(self.access_token, self.refresh_token)

        if self.first_boot:
            self.user = User.get_current_user(self.access_token)
            self.headerbar.set_title(f'Logged in as {self.user.id}')
            self.load_playlists()
            self.first_boot ^= self.first_boot

        self.__switch_widgets__(True)

    def dialog_clear(self):
        self.dialog_name_entry.set_text("")
        self.dialog_description_entry.set_text("")

    def add_clicked(self, *args):
        self.dialog_add = True
        self.dialog_clear()
        self.dialog.show_all()
        self.dialog.run()
        self.dialog.hide()

    def modify_clicked(self, *args):
        self.dialog_add = False
        model, it = self.tree_selection.get_selected()

        if it is not None:
            obj = model.get_value(it, 0)
            self.dialog_name_entry.set_text(obj.name)
            self.dialog_description_entry.set_text(obj.description)

            if obj.owner['id'] != self.user.id:
                self.dialog_vbox.set_sensitive(False)
                self.dialog_box.set_sensitive(False)

            self.dialog.show_all()
            self.dialog.run()

            self.dialog.hide()
            self.dialog_vbox.set_sensitive(True)
            self.dialog_box.set_sensitive(True)

    def dialog_apply_clicked(self, *args):
        if not self.check_entries():
            return

        self.__switch_spinner__(True)

        name = self.dialog_name_entry.props.text
        desc = self.dialog_description_entry.props.text

        if self.dialog_add:
            Playlist.new(name=name, description=desc, access_token=self.access_token,
                         user_id=self.user.id, ui_callback=self.add_entry_callback)
        else:
            model, it = self.tree_selection.get_selected()
            obj = model.get_value(it, 0)
            obj.update(name=name, desc=desc, access_token=self.access_token,
                       ui_callback=self.update_entry_callback)

    def dialog_cancel_clicked(self, *args):
        self.__switch_spinner__(False)
        self.dialog.hide()
        self.dialog_vbox.set_sensitive(True)
        self.dialog_box.set_sensitive(True)

    def check_entries(self):
        if self.dialog_name_entry.props.text == "" or \
           self.dialog_description_entry.props.text == "":
               return False
        return True

    def update_entry_callback(self, *args):
        self.__switch_spinner__(False)

        self.dialog.hide()
        self.dialog_vbox.set_sensitive(True)
        self.dialog_box.set_sensitive(True)

    def add_entry_callback(self, *args):
        playlist = Playlist(json.loads(args[0]))

        self.playlist_store.append((playlist,))

        self.__switch_spinner__(False)
        self.dialog.hide()
        self.dialog_vbox.set_sensitive(True)
        self.dialog_box.set_sensitive(True)

    def update_callback(self):
        self.__switch_spinner__(False)
        self.dialog.hide()
        self.dialog_vbox.set_sensitive(True)
        self.dialog_box.set_sensitive(True)

    def delete_clicked(self, *args):
        model, it = self.tree_selection.get_selected()
        if it is not None:
            obj = model.get_value(it, 0)
            obj.delete(self.access_token)
            model.remove(it)

    def check_auth(self):
        self.__switch_widgets__(False)
        logging.debug('Running check auth')
        get_tokens('me.mkls.PlaylistBrowser', self.__secret_callback__)

    def __secret_callback__(self,
                            token_type: str,
                            token: str) -> Union[Tuple[str, str], None]:
        if token_type not in ['access_token', 'refresh_token']:
            raise ValueError('Invalid token type.' +\
                             'Allowed: "refresh_token", "access_token"')

        self.token_counter += 1
        setattr(self, token_type, token)

        if self.token_counter == 2:
            logging.debug('Finished secret lookup')

            if self.access_token is None or self.refresh_token is None:
                logging.debug('Re-authenticating through browser')
                self.authenticate()
            else:
                refresh_tokens(self)
                # self.tokens_ready(self.access_token, self.refresh_token)

    def __switch_spinner__(self, toggle: bool):
        self.spinner.set_visible(toggle)
        if toggle:
            self.spinner.start()
        else:
            self.spinner.stop()

    def __switch_widgets__(self, toggle: bool):
        widgets = [
            self.add_button,
            self.modify_button,
            self.delete_button,
            self.scrolled_window
        ]

        for w in widgets:
            w.set_sensitive(toggle)

        self.__switch_spinner__(not toggle)

