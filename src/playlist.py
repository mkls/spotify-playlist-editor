import json
import logging
from gi.repository import Soup
from typing import List, Dict

from .globals import API_URL, read_data_stream

class Playlist:
    def __init__(self, j: dict):
        self.__dict__ = j

    def update_name(self, name: str):
        pass

    def update_desc(self, desc: str):
        pass

    def update(self, name: str, desc: str, access_token: str,
               ui_callback: callable):
        logging.debug(API_URL + f'playlists/{self.id}')
        msg = Soup.Message.new('PUT', API_URL + f'playlists/{self.id}')

        headers = msg.request_headers
        headers.append('Authorization', f'Bearer {access_token}')
        headers.append('Content-Type', 'application/json')
        headers.append('Accept', 'application/json')

        body = {
            'name': name,
            'description': desc
        }
        body = json.dumps(body)

        msg.set_request('application/json', Soup.MemoryUse.COPY,
                        body.encode('utf-8'))

        session = Soup.Session()
        session.send_async(msg, None, self.update_callback, ui_callback)

    @staticmethod
    def add_callback(source, result, *user_data):
        ui_callback = user_data[0]
        res = source.send_finish(result)
        res = read_data_stream(res)
        if res is None:
            logging.error('Failed to add playlist object')
        else:
            logging.debug(f'Added new playlist')
        ui_callback(res)

    def update_callback(self, source, result, *user_data):
        ui_callback = user_data[0]
        res = source.send_finish(result)
        data_stream = read_data_stream(res)

        if data_stream is None:
            logging.error("Failed to run update")
        else:
            logging.debug(f'ds: {data_stream}')
        ui_callback()

    @staticmethod
    def new(name: str, description: str, access_token: str, user_id: int,
            ui_callback: callable):
        msg = Soup.Message.new('POST', API_URL + f'users/{user_id}/playlists')

        headers = msg.request_headers
        headers.append('Authorization', f'Bearer {access_token}')
        headers.append('Content-Type', 'application/json')
        headers.append('Accept', 'application/json')

        body = {
            'name': name,
            'description': description
        }
        body = json.dumps(body)

        msg.set_request('application/json', Soup.MemoryUse.COPY,
                        body.encode('utf-8'))

        session = Soup.Session()
        session.send_async(msg, None, Playlist.add_callback, ui_callback)

    def set_public(self, public: bool):
        pass

    def set_collaborative(self, collaborative: bool):
        pass

    def delete_callback(self, source, result, *user_data):
        res = source.send_finish(result)

    def delete(self, access_token):
        msg = Soup.Message.new('DELETE', API_URL + f'playlists/{self.id}/followers')

        headers = msg.request_headers
        headers.append('Authorization', f'Bearer {access_token}')
        headers.append('Content-Type', 'application/json')
        headers.append('Accept', 'application/json')

        session = Soup.Session()
        session.send_async(msg, None, self.delete_callback)

    @staticmethod
    def from_json(j: str):
        playlist = Playlist(json.loads(j))

    @staticmethod
    def from_json_dict(j: dict):
        playlist = Playlist(j)

    @staticmethod
    def get_playlists(access_token: str, callback: callable):
        msg = Soup.Message.new('GET', API_URL + 'me/playlists')

        # headers = Soup.MessageHeaders.new(Soup.MessageHeadersType.REQUEST)
        headers = msg.request_headers
        headers.append('Authorization', f'Bearer {access_token}')
        headers.append('Content-Type', 'application/json')
        headers.append('Accept', 'application/json')

        session = Soup.Session()
        session.send_async(msg, None, callback)

