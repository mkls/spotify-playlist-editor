import os
import json
import logging

from gi.repository import Secret, Soup
from typing import Tuple

from .globals import *

NAME = 'me.mkls.PlaylistBrowser'

def get_schema(name: str) -> Secret.Schema:
    attributes = {
        'application': Secret.SchemaAttributeType.STRING,
        'type': Secret.SchemaAttributeType.STRING
    }
    schema = Secret.Schema.new(
        name=name,
        flags=Secret.SchemaFlags.NONE,
        attribute_names_and_types=attributes
    )

    return schema

def __on_token_stored__(source, result, *args):
    Secret.password_store_finish(result)

def __on_token_loaded__(source, result, *args):
    token_type, callback = args[0]
    token = Secret.password_lookup_finish(result)
    callback(token_type, token)

def save_tokens(access_token: str, refresh_token: str, schema_name: str = NAME):
    for t, v in [('access_token', access_token),
                 ('refresh_token', refresh_token)]:
        schema = get_schema(schema_name + '.' + t)
        attributes = {
            'application': NAME,
            'type': t
        }
        logging.debug(f'Saving {t}')
        Secret.password_store(schema, attributes, Secret.COLLECTION_DEFAULT,
                              t, v, None, __on_token_stored__)

def get_tokens(schema_name: str, callback: callable):
    for t in ['access_token', 'refresh_token']:
        schema = get_schema(schema_name + '.' + t)

        attributes = {
            'application': NAME,
            'type': t
        }
        logging.debug(f'Loading {t}')
        Secret.password_lookup(schema, attributes, None, __on_token_loaded__,
                               (t, callback))

def refresh_tokens(obj):
    post_msg = Soup.form_request_new_from_hash(
        method='POST',
        uri=AUTH_URL + 'api/token',

        form_data_set = {
            'client_id': os.environ['CLIENT_ID'],
            'grant_type': 'refresh_token',
            'refresh_token': obj.refresh_token
        }
    )
    session = Soup.Session()
    session.send_message(post_msg)

    data = post_msg.props.response_body.data
    data = json.loads(data)

    logging.debug(f'Refresh data: {data}')

    if 'error' in data:
        obj.authenticate()
    else:
        token = data['access_token']
        refresh_token = data['refresh_token']
        logging.debug('Got new tokens')
        obj.tokens_ready(token, refresh_token)

