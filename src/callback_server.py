import json
import os
import logging
import random
import webbrowser
from gi.repository import Soup
from base64 import b64encode
from hashlib import sha256
from string import ascii_letters, digits
from urllib.parse import urlencode, urlparse
from typing import Union, Dict, Callable

from .globals import *

logging.basicConfig(level=logging.DEBUG)

def random_str(source: str, length: int) -> str:
    return ''.join([
        random.SystemRandom().choice(source)
        for _ in range(length)
    ])

class CallbackServer:
    def __init__(self, port: int = 8888, ready_callback: Callable = None):
        self.port = port

        self.code_verifier = random_str(ascii_letters + digits, 64)
        self.code_challenge = self.code_verifier.encode('utf-8')
        self.code_challenge = b64encode(sha256(self.code_challenge).digest()).decode()

        for fr, to in [('+', '-'), ('/', '_'), ('=', '')]:
            self.code_challenge = self.code_challenge.replace(fr, to)

        self.state = None
        self.server = Soup.Server()
        self.server.add_handler('/callback', self.token_callback, None)

        self.ready_callback = ready_callback
        self.running = False

    def listen(self):
        logging.info('Starting callback server')

        success = self.server.listen_local(self.port,
                                           Soup.ServerListenOptions.IPV4_ONLY)
        if not success:
            raise RuntimeError(f'Failed to bind port {port}')

        self.running = True

    def disconnect(self):
        if self.running:
            logging.info('Shutting down the callback server')
            self.running = False
            self.server.disconnect()

    def __del__(self):
        self.disconnect()

    def token_callback(self, server: Soup.Server, msg: Soup.Message,
                       path: str, query: Union[Dict[str, str], None],
                       *user_data: Union[object, None]):
        token, refresh_token = None, None

        if msg.method != 'GET':
            msg.set_status(405)
            raise RuntimeError('Wrong HTTP method')

        if query['state'] != self.state:
            msg.set_status(400)
            raise RuntimeError('CSRF detected!')

        if 'error' in query:
            callback_message = 'You disallowed access :('
        else:
            post_msg = Soup.form_request_new_from_hash(
                method='POST',
                uri=AUTH_URL + 'api/token',
                form_data_set = {
                    'client_id': os.environ['CLIENT_ID'],
                    'grant_type': 'authorization_code',
                    'code': query['code'],
                    'redirect_uri': f'http://localhost:{self.port}/callback',
                    'code_verifier': self.code_verifier
                }
            )

            session = Soup.Session()
            status_code = session.send_message(post_msg)

            if status_code == 200:
                data = post_msg.props.response_body.data
                data = json.loads(data)

                token = data['access_token']
                refresh_token = data['refresh_token']
            else:
                logging.error(
                    f'Response failed: {post_msg.props.response_body.data}'
                )

            callback_message = 'Yay! Access allowed! You can return to the app now.'

        msg.set_response('text/html',Soup.MemoryUse.COPY,
                         CALLBACK_HTML.format(callback_message=callback_message).encode('utf-8'))
        msg.set_status(200)

        self.ready_callback(token, refresh_token)

    def get_token(self):
        self.state = random_str(ascii_letters + digits, 16)
        self.listen()

        params = {
            'client_id': os.environ['CLIENT_ID'],
            'response_type': 'code',
            'redirect_uri': f'http://localhost:{self.port}/callback',
            'code_challenge_method': 'S256',
            'code_challenge': self.code_challenge,
            'scope': ' '.join(SCOPES),
            'state': self.state
        }

        params_str = '?' + urlencode(params)
        url = AUTH_URL + 'authorize' + params_str

        webbrowser.open_new_tab(url)
