import logging
from gi.repository import Gio

AUTH_URL = 'https://accounts.spotify.com/'
API_URL = 'https://api.spotify.com/v1/'
SCOPES = [
    'playlist-modify-private',
    'playlist-read-private',
    'playlist-modify-public',
    'playlist-read-collaborative'
]
CALLBACK_HTML = '''
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
        <title></title>
    </head>
    <body>
        <h1>{callback_message}</h1>
    </body>
    </html>
'''

def read_data_stream(stream):
    lines = []
    if stream:
        data_input_stream = Gio.DataInputStream.new(stream)
        while True:
            line, length = data_input_stream.read_line_utf8()
            if line is None:
                break
            else:
                lines.append(line)
    else:
        return None
    j = "".join(lines)
    return j

