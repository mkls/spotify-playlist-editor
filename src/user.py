import json
import logging
from gi.repository import Soup

from .globals import *

class User:
    def __init__(self, j: dict):
        self.__dict__ = j

    @staticmethod
    def get_current_user(access_token: str):
        msg = Soup.Message.new('GET', API_URL + 'me')

        # headers = Soup.MessageHeaders.new(Soup.MessageHeadersType.REQUEST)
        headers = msg.request_headers
        headers.append('Authorization', f'Bearer {access_token}')
        headers.append('Content-Type', 'application/json')
        headers.append('Accept', 'application/json')

        session = Soup.Session()
        status_code = session.send_message(msg)

        user = None

        if status_code == 200:
            data = msg.props.response_body.data
            user = User(json.loads(data))
        else:
            logging.error(f'Failed getting current user: {msg.props.response_body.data}')

        return user

    @staticmethod
    def from_json(j: str):
        user = User(json.loads(j))

    @staticmethod
    def from_json_dict(j: dict):
        user = User(j)
